from django.db import models
from django.contrib.auth.models import User

from google2authtest.apps.utils import get_secret


class TwoAuth(models.Model):
    fk_user = models.ForeignKey(User, verbose_name=u'ユーザー', related_name='two_auth', on_delete=models.SET_NULL, null=True)
    secret_key = models.CharField(verbose_name=u'認証キー', max_length=16)

    @classmethod
    def get_secret_key(cls, user):
        try:
            two_auth = cls.objects.get(fk_user=user)
        except cls.DoesNotExist:
            return None

        return two_auth.secret_key
