from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.shortcuts import render
from django.http.response import JsonResponse

from google2authtest.apps.utils import *
from cms.models.two_auth import TwoAuth


@login_required()
def main(request):
    """
    メイン画面を表示する
    """

    return render(request, 'main/main.html')


@login_required()
@transaction.atomic
def display_qrcode(request):
    user_id = request.user.email
    secret = TwoAuth.get_secret_key(request.user)
    if not secret:
        secret = get_secret()
        two_auth = TwoAuth(fk_user=request.user, secret_key=secret)
        two_auth.save()

    return JsonResponse({
        'img':  get_image_b64(get_auth_url(user_id, secret)),
    })
