from django.conf.urls import url
from google2authtest.apps.main.views import *

urlpatterns = [
    url(r'^$', main, name='main'),
    url(r'^display_qrcode$', display_qrcode, name='display_qrcode'),
]
