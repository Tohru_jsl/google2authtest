import warnings

from django.contrib.auth import authenticate, views as auth_views, REDIRECT_FIELD_NAME
from django.contrib.auth.forms import AuthenticationForm
from django.http.response import JsonResponse
from django.shortcuts import render
from django.utils.deprecation import RemovedInDjango21Warning

from google2authtest.apps.utils import *
from cms.models.two_auth import TwoAuth


def login(request, template_name='registration/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          extra_context=None, redirect_authenticated_user=False):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        secret = TwoAuth.get_secret_key(user)
        if secret:
            auth_token = request.POST['auth_token']
            is_valid = otp.valid_totp(token=int(auth_token), secret=secret)
        else:
            is_valid = True
        if not is_valid:
            form = AuthenticationForm(request.POST)
            return render(request, template_name, context={'form': form})

    warnings.warn(
        'The login() view is superseded by the class-based LoginView().',
        RemovedInDjango21Warning, stacklevel=2
    )
    return auth_views.LoginView.as_view(
        template_name=template_name,
        redirect_field_name=redirect_field_name,
        form_class=authentication_form,
        extra_context=extra_context,
        redirect_authenticated_user=redirect_authenticated_user,
    )(request)


def user_check(request):
    is_check_ok = False
    is_two_auth = True
    username = request.POST['username']
    password = request.POST['password']

    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            is_check_ok = True
            is_two_auth = True if TwoAuth.get_secret_key(user) else False

    return JsonResponse({
        'is_check_ok': is_check_ok,
        'is_two_auth': is_two_auth,
    })
