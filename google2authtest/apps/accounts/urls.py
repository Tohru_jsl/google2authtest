from django.conf.urls import url
from django.contrib.auth import views as auth_views

from google2authtest.apps.accounts import views as accounts_views

urlpatterns = [
    url(r'^login/$', accounts_views.login,
        {'template_name': 'accounts/login.html'}, name="login"),
    url(r'^logout/$', auth_views.logout,
        {'template_name': 'accounts/logout.html'}, name="logout"),
    url(r'^user_check/$', accounts_views.user_check, name="user_check"),
]
